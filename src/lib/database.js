import mysql from "serverless-mysql";
import knex from "knex";

export const queryBuilder = knex({
	client: "mysql"
});

export const database = mysql({
	config: {
		host: process.env.MYSQL_HOST,
		user: process.env.MYSQL_USERNAME,
		password: process.env.MYSQL_PASSWORD,
		database: process.env.MYSQL_DATABASE,
		port: parseInt(process.env.MYSQL_PORT, 10)
	}
});

export const query = async (q, values) => {
	try {
		let results = await database.query(q, values);
		await database.end();

		return Promise.resolve(results);
	} catch (err) {
		return Promise.reject(err);
	}
};
