import {internalServerError, methodNotAllowed} from "../../lib/responses";
import {getStatistics} from "../../helpers/wakatime";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let statsObject = await getStatistics();

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				stats: statsObject
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default handler;
