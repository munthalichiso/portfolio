import {
	query,
	queryBuilder
} from "../../../../lib/database";
import {internalServerError, methodNotAllowed} from "../../../../lib/responses";
import authMiddleware from "../../../../middleware/authMiddleware";
import RegisterWebauthnSchema from "../../../../schemas/API/RegisterWebauthnSchema";
import base64url from "base64url";
import {verifyAuthenticatorAttestationResponse} from "../../../../helpers/webauthn";

const handler = async (req, res) => {
	try {
		if (req.method === "POST") {
			let {body} = req;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let validationResult = RegisterWebauthnSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				response
			} = validationResult.value;

			let userQuery = queryBuilder.table("users")
				.where({
					id: req.userId
				})
				.toSQL();

			let [user] = await query(userQuery.sql, userQuery.bindings);
			let userChallenge = user.webauthnChallenge;

			let clientData = JSON.parse(base64url.decode(response.clientDataJSON));

			if (clientData.challenge !== userChallenge) {
				return res.status(400).send({
					error: true,
					displayMessage: "Challenges do not match",
					code: "CHALLENGE_MISMATCH"
				});
			}

			let result;

			try {
				if (response.attestationObject) {
					result = await verifyAuthenticatorAttestationResponse(response);

					if (result.verified) {
						let updateUserQuery = queryBuilder.table("users")
							.update({
								webauthnLoginEnabled: true,
								webauthnChallenge: null,
								webauthnAuthrInfo: JSON.stringify(result.authrInfo)
							})
							.where({
								id: user.id
							})
							.toSQL();

						await query(updateUserQuery.sql, updateUserQuery.bindings);
					}
				} else {
					return res.status(422).send({
						error: true,
						displayMessage: "Failed to determine response type",
						code: "UNKNOWN_RESPONSE_TYPE"
					});
				}
			} catch (err) {
				return res.status(400).send({
					error: true,
					displayMessage: "Failed to authenticate",
					code: "AUTHENTICATION_FAILED"
				});
			}

			if (result.verified) {
				return res.status(200).send({
					error: false,
					displayMessage: "Successfully verified",
					code: "SUCCESS"
				});
			}

			return res.status(400).send({
				error: true,
				displayMessage: "Verification failed",
				code: "VERIFICATION_FAILED"
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
