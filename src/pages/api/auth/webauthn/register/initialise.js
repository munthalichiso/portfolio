import {
	query,
	queryBuilder
} from "../../../../../lib/database";
import {internalServerError, methodNotAllowed} from "../../../../../lib/responses";
import authMiddleware from "../../../../../middleware/authMiddleware";
import {generateServerChallenge} from "../../../../../helpers/webauthn";

const handler = async (req, res) => {
	try {
		if (req.method === "POST") {
			let {userId} = req;

			let userQuery = queryBuilder.table("users")
				.select("username")
				.where({
					id: userId
				})
				.toSQL();

			let [user] = await query(userQuery.sql, userQuery.bindings);
			let {username} = user;

			let serverChallenge = generateServerChallenge(userId, username);

			let userUpdateQuery = queryBuilder.table("users")
				.update({
					webauthnChallenge: serverChallenge.challenge
				})
				.where({
					id: userId
				})
				.toSQL();

			await query(userUpdateQuery.sql, userUpdateQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				challenge: serverChallenge
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
