import {
	query,
	queryBuilder
} from "../../../../lib/database";
import {internalServerError, methodNotAllowed} from "../../../../lib/responses";
import authMiddleware from "../../../../middleware/authMiddleware";

const handler = async (req, res) => {
	try {
		if (req.method === "DELETE") {
			let updateQuery = queryBuilder.table("users")
				.update({
					webauthnLoginEnabled: 0,
					webauthnChallenge: null,
					webauthnAuthrInfo: null,
					webauthnLoginChallenge: null
				})
				.where({
					id: req.userId
				})
				.toSQL();

			await query(updateQuery.sql, updateQuery.bindings);

			return res.status(200).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
