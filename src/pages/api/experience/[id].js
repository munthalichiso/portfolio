import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	notFound,
	methodNotAllowed
} from "../../../lib/responses";
import authMiddleware from "../../../middleware/authMiddleware";

const handler = async (req, res) => {
	try {
		if (req.method === "DELETE") {
			let {id} = req.query;

			let experienceQuery = queryBuilder.table("experience")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [experience] = await query(experienceQuery.sql, experienceQuery.bindings);

			if (!experience)
				return res.status(404).send(notFound("Experience"));

			let deleteQuery = queryBuilder.table("experience")
				.del()
				.where({
					id
				})
				.toSQL();

			await query(deleteQuery.sql, deleteQuery.bindings);

			return res.status(204).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["DELETE"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler);
