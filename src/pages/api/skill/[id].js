import {
	query,
	queryBuilder
} from "../../../lib/database";
import {
	internalServerError,
	notFound,
	methodNotAllowed
} from "../../../lib/responses";
import {toTitleCase} from "../../../helpers/general";
import EditSkillSchema from "../../../schemas/API/EditSkillSchema";
import authMiddleware from "../../../middleware/authMiddleware";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let {id} = req.query;

			let skillQuery = queryBuilder.table("skills")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [skill] = await query(skillQuery.sql, skillQuery.bindings);

			if (!skill)
				return res.status(404).send(notFound("Skill"));

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				skill
			});
		}

		if (req.method === "PUT") {
			let {body} = req;
			let {id} = req.query;

			if (!body) {
				return res.status(400).send({
					error: true,
					displayMessage: "Missing body",
					code: "MISSING_BODY"
				});
			}

			if (Object.keys(body).length === 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "Request body is empty",
					code: "EMPTY_BODY"
				});
			}

			let skillsQuery = queryBuilder.table("skills")
				.select("*")
				.where({
					id
				})
				.toSQL();

			let [skill] = await query(skillsQuery.sql, skillsQuery.bindings);

			if (!skill)
				return res.status(404).send(notFound("Skill"));

			let validationResult = EditSkillSchema.validate(body, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the body",
					errors: validationResult.error.details,
					code: "BODY_VALIDATION_ERROR"
				});
			}

			let {
				name,
				type,
				link,
				imageUrl,
				colour
			} = validationResult.value;

			skill.name = name;
			skill.type = toTitleCase(type);
			skill.link = link;
			skill.imageUrl = imageUrl;
			skill.colour = colour;

			let updateQuery = queryBuilder.table("skills")
				.update({
					name: skill.name,
					link: skill.link,
					type: skill.type,
					imageUrl: skill.imageUrl,
					colour: skill.colour
				})
				.where({
					id
				})
				.toSQL();

			await query(updateQuery.sql, updateQuery.bindings);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				skill
			});
		}

		if (req.method === "DELETE") {
			let {id} = req.query;

			let skillQuery = queryBuilder.table("skills")
				.leftJoin("projectSkills", "skills.id", "projectSkills.skillId")
				.select("skills.id")
				.count("projectSkills.id as projectCount")
				.where({
					"skills.id": id
				})
				.toSQL();

			let [skill] = await query(skillQuery.sql, skillQuery.bindings);

			if (!skill)
				return res.status(404).send(notFound("Skill"));

			if (skill.projectCount > 0) {
				return res.status(400).send({
					error: true,
					displayMessage: "This skill is currently being used by one or more projects, " +
						"therefore cannot be deleted",
					code: "USED_IN_PROJECTS"
				});
			}

			let deleteQuery = queryBuilder.table("skills")
				.del()
				.where({
					id
				})
				.toSQL();

			await query(deleteQuery.sql, deleteQuery.bindings);

			return res.status(204).send();
		}

		return res.status(405).send(methodNotAllowed(req.method, ["GET", "PUT", "DELETE"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
