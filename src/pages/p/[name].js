import React from "react";
import serverSideGetProject from "../../helpers/apiMethods/projects/serverSideGetProject";
import ErrorPage from "../../components/Errors/ErrorPage";

const ProjectName = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return null;
};

export const getServerSideProps = async (ctx) => {
	const {name} = ctx.params;
	const {body, status} = await serverSideGetProject(name);

	if (status === 404)
		return {props: {error: 404}};

	if (status !== 200)
		return {props: {error: 500}};

	const {project} = body;

	if (!project)
		return {props: {error: 404}};

	return {
		redirect: {
			permanent: false,
			destination: `/projects/${project.id}`
		}
	};
};

export default ProjectName;
