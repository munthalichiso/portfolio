import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import SkillsList from "../components/Skills/SkillsList";

const Skills = () => {
	return (
		<>
			<SmallHeader
				title="Skills"
				subtitle={`A list of languages, frameworks, tools and platforms that I am
					familar with, and used within projects`}
			/>

			<SkillsList/>
		</>
	);
};

export default Skills;
