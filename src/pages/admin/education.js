import React, {useEffect} from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import AdminEducationList from "../../components/Admin/Education/AdminEducationList";
import {useRouter} from "next/router";
import {useAlert} from "react-alert";
import {useAuth} from "../../lib/useAuth";
import ErrorPage from "../../components/Errors/ErrorPage";

const Education = ({error}) => {
	const router = useRouter();
	const alert = useAlert();

	if (error)
		return <ErrorPage statusCode={error}/>;

	useEffect(() => {
		let {addedEducation} = router.query;

		if (addedEducation === "1") {
			alert.success("Successfully added new education");
			window.history.replaceState(null, null, router.pathname);
		}
	}, []);

	return (
		<>
			<SmallHeader
				title="Manage Education"
			/>

			<AdminEducationList/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default Education;
