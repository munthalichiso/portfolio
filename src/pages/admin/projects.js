import React, {useEffect} from "react";
import SmallHeader from "../../components/Header/SmallHeader";
import AdminProjectsList from "../../components/Admin/Projects/AdminProjectsList";
import {useRouter} from "next/router";
import {useAlert} from "react-alert";
import {useAuth} from "../../lib/useAuth";
import ErrorPage from "../../components/Errors/ErrorPage";

const Projects = ({error}) => {
	const router = useRouter();
	const alert = useAlert();

	if (error)
		return <ErrorPage statusCode={error}/>;

	useEffect(() => {
		let {addedProject, editedProject} = router.query;

		if (addedProject === "1") {
			alert.success("Successfully added new project");
			window.history.replaceState(null, null, router.pathname);
		}

		if (editedProject === "1") {
			alert.success("Successfully edited project");
			window.history.replaceState(null, null, router.pathname);
		}
	}, []);

	return (
		<>
			<SmallHeader
				title="Manage Projects"
			/>

			<AdminProjectsList/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default Projects;
