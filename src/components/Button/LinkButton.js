import React from "react";
import Link from "next/link";

const LinkButton = ({children, href}) => {
	return (
		<Link href={href} passHref>
			<a
				className="bg-dark-750 font-semibold text-white p-2 px-3
					w-32 rounded-full hover:bg-dark-600 shadow-md hover:shadow-none
					transition-all duration-300 m-2 cursor-pointer w-max inline-block"
			>
				{children}
			</a>
		</Link>
	);
};

export default LinkButton;
