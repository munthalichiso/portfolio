import React from "react";
import {ChevronRightIcon} from "@heroicons/react/solid";

const PageMoveRightButton = ({onClick, disabled}) => {
	if (disabled) {
		return (
			<a
				className="relative inline-flex items-center px-2 py-2 rounded-r-md opacity-50
					bg-dark-700 border-dark-600 border text-sm font-medium text-white">
				<ChevronRightIcon className="h-5 w-5"/>
			</a>
		);
	}

	return (
		<a
			className="relative inline-flex items-center px-2 py-2 rounded-r-md cursor-pointer
			   bg-dark-700 border-dark-600 border text-sm font-medium text-white hover:bg-dark-750"
			onClick={onClick}
		>
			<ChevronRightIcon className="h-5 w-5"/>
		</a>
	);
};

export default PageMoveRightButton;
