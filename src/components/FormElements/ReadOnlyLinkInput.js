import React from "react";
import Link from "next/link";

const ReadOnlyLinkInput = ({href, target = "_blank"}) => {
	return (
		<div className={`form-input appearance-none rounded-none block w-full px-4
			py-3 bg-dark-650 border border-dark-500 placeholder-gray-200 text-white
			focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm
			disabled:opacity-60 rounded-t-md rounded-b-md text-left text-blue-500`}>
			<Link href={href} passHref>
				<a target={target}>{href}</a>
			</Link>
		</div>
	);
};

export default ReadOnlyLinkInput;
