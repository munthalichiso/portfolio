import React from "react";

const BulletListItem = ({children}) => {
	return (
		<li className="font-bold text-xl">
			{children}
		</li>
	);
};

export default BulletListItem;
