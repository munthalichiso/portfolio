import React, {useState} from "react";
import ProfileButton from "./ProfileButton";
import ProfileMenu from "./ProfileMenu";

const Profile = () => {
	const [visible, setVisible] = useState(false);

	const handleButtonClick = (event) => {
		event.preventDefault();

		setVisible(!visible);
	};

	const handleUnfocus = (event) => {
		event.preventDefault();

		setVisible(false);
	};

	return (
		<>
			<ProfileButton onClick={handleButtonClick}/>
			<ProfileMenu visible={visible} onBlur={handleUnfocus}/>
		</>
	);
};

export default Profile;
