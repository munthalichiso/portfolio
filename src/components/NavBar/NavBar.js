import React, {useState} from "react";
import MobileMenuButton from "./MobileMenuButton";
import NavBarItem from "./NavBarItem";
import {useRouter} from "next/router";
import {pages} from "../../lib/constants";
import Profile from "./Profile";
import MobileNavMenu from "./MobileNavMenu";

const NavBar = () => {
	const router = useRouter();
	const [showMobileMenu, setShowMobileMenu] = useState(false);

	let {asPath} = router;
	let currentPath = asPath.split("?")[0];

	const handleMobileMenuButtonClicked = (event) => {
		event.preventDefault();
		setShowMobileMenu(!showMobileMenu);
	};

	return (
		<nav className="bg-dark-650">
			<div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
				<div className="relative flex items-center justify-between h-16">
					<div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
						<MobileMenuButton onClick={handleMobileMenuButtonClicked}/>
					</div>
					<div className="flex-1 flex items-center justify-center sm:items-stretch
						sm:justify-start">
						<div className="hidden sm:block sm:ml-6">
							<div className="flex space-x-4">
								{pages.map(page => {
									return <NavBarItem
										key={page.url}
										text={page.name}
										href={page.url}
										active={currentPath === page.url}
									/>;
								})}
							</div>
						</div>
					</div>
					<div className="absolute inset-y-0 right-0 flex items-center pr-2
						sm:static sm:inset-auto sm:ml-6 sm:pr-0">
						<div className="ml-3 relative">
							<Profile/>
						</div>
					</div>
				</div>
			</div>

			{showMobileMenu ? <MobileNavMenu/> : ""}
		</nav>
	);
};

export default NavBar;
