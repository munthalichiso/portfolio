import React from "react";
import {CheckIcon, ExclamationIcon, InformationCircleIcon} from "@heroicons/react/solid";

const Toast = ({body, type}) => {
	let title;
	let colour;
	let icon;

	switch (type) {
		case "error": {
			icon = <ExclamationIcon/>;
			colour = "red-500";
			title = "Error!";
			break;
		}

		case "success": {
			icon = <CheckIcon/>;
			colour = "green-500";
			title = "Success!";
			break;
		}

		default: {
			icon = <InformationCircleIcon/>;
			colour = "cyan-400";
			title = "Info!";
			break;
		}
	}

	return (
		<div
			className="top-0 right-0 fixed z-50 w-full md:max-w-sm p-4
				md:p-4 max-h-screen overflow-hidden pointer-events-none">
			<div className="flex-1 flex-col fade w-full mr-8 justify-end pointer-events-none">
				<div className="flex py-1 w-full transform transition-all duration-300 pointer-events-auto">
					<div
						className={`flex w-full visible flex-row shadow-lg border border-l-4 rounded-md
							duration-100 cursor-pointer transform transition-all hover:scale-102 bg-dark-650
							border-${colour} max-h-40`}>
						<div className="flex flex-row p-2 flex-no-wrap w-full">
							<div className={`flex items-center h-12 w-12 mx-auto text-xl
								select-none text-${colour}`}>
								{icon}
							</div>
							<div className="flex flex-col flex-no-wrap px-1 w-full">
								<div className="flex my-auto font-bold select-none text-white">
									{title}
								</div>
								<p className="-mt-0.5 my-auto break-words flex text-gray-100
									text-sm truncate-1-lines">
									{body}
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Toast;
