import React from "react";
import Header from "../Header/Header";

const ErrorPage = ({statusCode}) => {
	let heading;
	let description;

	if (statusCode === 404) {
		heading = "404 • Not Found";
		description = "This page could not be found";
	} else if (statusCode === 403 || statusCode === 401) {
		heading = `${statusCode} • Unauthorized`;
		description = "It looks like you don't have access to this page";
	} else {
		heading = `${statusCode ? statusCode : 500} • Internal Server Error`;
		description = "Something went wrong when loading this page";
	}

	return (
		<Header
			title={heading}
			subtitle={description}
		/>
	);
};

export default ErrorPage;
