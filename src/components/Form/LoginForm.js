import React, {useState} from "react";
import LoginSchema from "../../schemas/Frontend/LoginSchema";
import {useRouter} from "next/router";
import Button from "../Button/Button";
import PasswordLoginForm from "./LoginForm/PasswordLoginForm";
import PasswordlessLoginForm from "./LoginForm/PasswordlessLoginForm";
import login from "../../helpers/apiMethods/login";
import loginWebauthn from "../../helpers/apiMethods/auth/webauthn/login";
import {useAlert} from "react-alert";
import webauthnLoginResponse from "../../helpers/apiMethods/auth/webauthn/loginResponse";
import {publicKeyCredentialToJson} from "../../helpers/webauthn";
import {mutate} from "swr";

const LoginForm = () => {
	const alert = useAlert();
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const [loginType, setLoginType] = useState("password");

	const handleWebauthnLogin = async (event) => {
		event.preventDefault();

		let {
			username: usernameElement
		} = event.target;

		let username = usernameElement.value;

		if (!username) {
			setErrors([{message: "Username cannot be empty"}]);
			return;
		}

		setErrors([]);
		setLoading(true);

		let {body, error} = await loginWebauthn(username);

		if (error) {
			setLoading(false);
			setErrors([{message: error.displayMessage}]);
			return;
		}

		let {challenge} = body;

		try {
			let response = await navigator.credentials.get({
				publicKey: {
					allowCredentials: challenge.allowCredentials.map(credentials => {
						return {
							type: credentials.type,
							id: Buffer.from(credentials.id, "base64"),
							transports: ["internal"]
						};
					}),
					challenge: Buffer.from(challenge.challenge, "base64"),
					userVerification: "required"
				}
			});

			let bodyJson = publicKeyCredentialToJson(response);
			bodyJson.username = username;

			let {error: loginResponseError} = await webauthnLoginResponse(bodyJson);

			setLoading(false);

			if (loginResponseError) {
				alert.error(loginResponseError.displayMessage);
				return;
			}

			await mutate("/api/me");
			await router.push("/?loggedIn=1");
		} catch (err) {
			alert.error(err.message);
		}

		setLoading(false);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		if (loginType === "webauthn") {
			await handleWebauthnLogin(event);
			return;
		}

		let {
			username: usernameElement,
			password: passwordElement
		} = event.target;

		let username = usernameElement.value;
		let password = passwordElement.value;

		let validationResult = LoginSchema.validate({
			username,
			password
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			username,
			password
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await login(username, password);

		if (error) {
			setErrors([{
				message: error.displayMessage
			}]);
			setLoading(false);
			return;
		}

		await router.push("/?loggedIn=1");
	};

	return (
		<div className="flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				{loginType === "password" ?
					<Button colour="pink" onClick={() => setLoginType("webauthn")}>
						Switch To Passwordless Login
					</Button> :
					<Button colour="pink" onClick={() => setLoginType("password")}>
						Sign In With Password
					</Button>
				}

				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<input type="hidden" name="remember" value="true"/>
					{loginType === "password" ?
						<PasswordLoginForm loading={loading} errors={errors}/> :
						<PasswordlessLoginForm loading={loading} errors={errors}/>
					}
				</form>
			</div>
		</div>
	);
};

export default LoginForm;
