import React, {useState} from "react";
import TextInput from "../FormElements/TextInput";
import {LinkIcon, PencilAltIcon, PhotographIcon, ViewListIcon} from "@heroicons/react/solid";
import ColourPickerButton from "../ColourPicker/ColourPickerButton";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import {useRouter} from "next/router";
import CreateSkillSchema from "../../schemas/Frontend/CreateSkillSchema";
import editSkill from "../../helpers/apiMethods/skills/editSkill";
import {mutate} from "swr";

const EditSkillForm = ({onChange, baseSkill}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const [colour, setColour] = useState(baseSkill.colour);

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		onChange({name, value});
	};

	const handleColourChange = (newColour) => {
		setColour(newColour);

		onChange({name: "colour", value: newColour});
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			name: nameElement,
			type: typeElement,
			link: linkElement,
			imageUrl: imageUrlElement
		} = event.target;

		let name = nameElement.value;
		let type = typeElement.value;
		let link = linkElement.value;
		let imageUrl = imageUrlElement.value;

		// The create schema will have the same properties
		let validationResult = CreateSkillSchema.validate({
			name,
			type,
			link,
			imageUrl
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			name,
			type,
			link,
			imageUrl
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await editSkill(baseSkill.id, name, type, link, imageUrl, colour);

		if (error) {
			setErrors([{message: error.displayMessage}]);
			return;
		}

		await mutate("/api/skill");
		await mutate(`/api/skill/${baseSkill.id}`);

		setLoading(false);

		await router.push("/admin/skills?editedSkill=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<TextInput
						name="name"
						placeholder="Name"
						position="alone"
						autoComplete="off"
						icon={PencilAltIcon}
						onInput={handleOnInput}
						defaultValue={baseSkill.name}
					/>

					<TextInput
						name="type"
						placeholder="Type"
						position="alone"
						autoComplete="off"
						icon={ViewListIcon}
						onInput={handleOnInput}
						defaultValue={baseSkill.type}
					/>

					<TextInput
						name="link"
						placeholder="Link"
						position="alone"
						autoComplete="off"
						icon={LinkIcon}
						onInput={handleOnInput}
						defaultValue={baseSkill.link}
					/>

					<TextInput
						name="imageUrl"
						placeholder="Image URL"
						position="alone"
						autoComplete="off"
						icon={PhotographIcon}
						onInput={handleOnInput}
						defaultValue={baseSkill.imageUrl}
					/>

					<ColourPickerButton
						name="colour"
						colour={colour}
						setColour={handleColourChange}
					/>

					<ErrorList errors={errors}/>

					<SubmitButton
						text={loading ? "Saving..." : "Save"}
						loading={loading}
					/>
				</form>
			</div>
		</div>
	);
};

export default EditSkillForm;
