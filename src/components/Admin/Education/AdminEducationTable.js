import React from "react";
import TableHeader from "../../Table/TableHeader";
import TableRow from "../../Table/TableRow";
import TableCell from "../../Table/TableCell";
import Button from "../../Button/Button";

const AdminEducationTable = ({education, onEducationDeleteClick}) => {
	return (
		<div className="flex flex-col mx-4 mb-6 mt-6 lg:mx-32">
			<div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div className="shadow overflow-hidden border-b border-dark-200 rounded-lg">
						<table className="min-w-full divide-y divide-dark-200">
							<thead>
								<tr>
									<TableHeader
										text="Subject"
									/>
									<TableHeader
										text="Level"
									/>
									<TableHeader
										text="Location"
									/>
									<TableHeader
										text="Grade"
									/>
									<TableHeader
										text="Start/End Date"
									/>
									<TableHeader
										text="Actions"
									/>
								</tr>
							</thead>
							<tbody className="divide-y divide-dark-200">
								{education.map((educationObject) => (
									<TableRow key={educationObject.id}>
										<TableCell>
											{educationObject.subject}
										</TableCell>

										<TableCell>
											{educationObject.level}
										</TableCell>

										<TableCell>
											{educationObject.location}
										</TableCell>

										<TableCell>
											{educationObject.grade}
										</TableCell>

										<TableCell>
											{educationObject.startDate} - {educationObject.endDate}
										</TableCell>

										<TableCell>
											<Button
												colour="red"
												textColour="white"
												onClick={() => onEducationDeleteClick(educationObject.id)}
											>
												Delete
											</Button>
										</TableCell>
									</TableRow>
								))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

export default AdminEducationTable;
