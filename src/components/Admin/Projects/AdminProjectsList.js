import React, {useState} from "react";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import {useAlert} from "react-alert";
import {mutate} from "swr";
import Button from "../../Button/Button";
import AdminProjectTable from "./AdminProjectTable";
import DeleteProjectModal from "../../Modal/DeleteProjectModal";
import deleteProject from "../../../helpers/apiMethods/projects/deleteProject";

const AdminProjectsList = () => {
	const alert = useAlert();
	const {projects, loading: projectsLoading} = useEndpoint("/api/project/all", "projects",
		true, false);

	const [isOpen, setIsOpen] = useState(false);
	const [selectedProjectToDelete, setSelectedProjectToDelete] = useState(false);
	const [projectDeleteLoading, setProjectDeleteLoading] = useState(false);

	if (projectsLoading) {
		return (
			<div className="text-center px-8 gap-4 lg:px-32">
				{Array(12).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="3rem"
					/>;
				})}
			</div>
		);
	}

	const handleProjectDeleteClicked = (projectId) => {
		setIsOpen(true);
		setSelectedProjectToDelete(projectId);
	};

	const handleProjectDeleteConfirm = async () => {
		setProjectDeleteLoading(true);

		let {error} = await deleteProject(selectedProjectToDelete);

		if (error) {
			setProjectDeleteLoading(false);
			alert.error(`Failed to delete project: ${error.displayMessage}`);
			return;
		}

		await mutate("/api/project/all");
		setProjectDeleteLoading(false);
		setIsOpen(false);
		alert.success("Successfully deleted project");
	};

	return (
		<>
			<div className="mb-1 ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4 lg:px-6 lg:mb-8">
				<Button colour="green" href="/admin/projects/new">
					Create New
				</Button>
			</div>

			<AdminProjectTable
				projects={projects}
				onProjectDeleteClick={handleProjectDeleteClicked}
			/>

			<DeleteProjectModal
				isOpen={isOpen}
				onClose={() => setIsOpen(false)}
				onDelete={handleProjectDeleteConfirm}
				deleteLoading={projectDeleteLoading}
			/>
		</>
	);
};

export default AdminProjectsList;
