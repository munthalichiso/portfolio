import React, {useState} from "react";
import SkillTag from "../../Skills/SkillTag";
import AddSkillTagModal from "../../Modal/AddSkillTagModal";
import useEndpoint from "../../../lib/useEndpoint";
import Button from "../../Button/Button";

const EditableSkillsList = ({onChange, defaultSkills = []}) => {
	const {skills, loading: skillsLoading} = useEndpoint("/api/skill", "skills", true, false);

	const [addSkillOpen, setAddSkillOpen] = useState(false);
	const [selectedSkills, setSelectedSkills] = useState(defaultSkills);

	const handleAddSkills = (addedSkills) => {
		let newSelectedSkills = [...selectedSkills, ...addedSkills];
		setSelectedSkills(newSelectedSkills);
		setAddSkillOpen(false);
		onChange(newSelectedSkills);
	};

	return (
		<div className="mt-4">
			<h1 className="font-bold text-left text-xl">
				Skills
			</h1>

			<div>
				{selectedSkills.map(skillId => {
					if (skillsLoading)
						return null;

					let skill = skills.find(x => x.id === skillId);

					return <SkillTag
						key={skill.id}
						text={skill.name}
						colour={skill.colour}
						clickable={false}
						showDeleteIcon={true}
						onDelete={() => {
							let newSelectedSkills = selectedSkills.filter(x => x !== skillId);
							setSelectedSkills(newSelectedSkills);
							onChange(newSelectedSkills);
						}}
					/>;
				})}

				<div className="block mt-4">
					<Button colour="green" onClick={() => setAddSkillOpen(true)}>
						Add Skill
					</Button>
				</div>
			</div>

			<AddSkillTagModal
				isOpen={addSkillOpen}
				onClose={() => setAddSkillOpen(false)}
				onAdd={handleAddSkills}
				existingSkills={selectedSkills}
			/>
		</div>
	);
};

export default EditableSkillsList;
