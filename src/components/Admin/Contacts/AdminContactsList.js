import React, {useState} from "react";
import {useAlert} from "react-alert";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import {mutate} from "swr";
import Button from "../../Button/Button";
import AdminContactsTable from "./AdminContactsTable";
import DeleteContactModal from "../../Modal/DeleteContactModal";
import deleteContact from "../../../helpers/apiMethods/contacts/deleteContact";

const AdminContactsList = () => {
	const alert = useAlert();
	const {contacts, loading: contactsLoading} = useEndpoint("/api/contact", "contacts",
		true, false);

	const [isOpen, setIsOpen] = useState(false);
	const [selectedContactToDelete, setSelectedContactToDelete] = useState(false);
	const [contactDeleteLoading, setContactDeleteLoading] = useState(false);

	if (contactsLoading) {
		return (
			<div className="text-center px-8 gap-4 lg:px-32">
				{Array(12).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="3rem"
					/>;
				})}
			</div>
		);
	}

	const handleContactDeleteClicked = (contactId) => {
		setIsOpen(true);
		setSelectedContactToDelete(contactId);
	};

	const handleContactDeleteConfirm = async () => {
		setContactDeleteLoading(true);

		let {error} = await deleteContact(selectedContactToDelete);

		if (error) {
			setContactDeleteLoading(false);
			alert.error(`Failed to delete contact: ${error.displayMessage}`);
			return;
		}

		await mutate("/api/contact");
		setContactDeleteLoading(false);
		setIsOpen(false);
		alert.success("Successfully deleted contact");
	};

	return (
		<>
			<div className="mb-1 ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4 lg:px-6 lg:mb-8">
				<Button colour="green" href="/admin/contacts/new">
					Create New
				</Button>
			</div>

			<AdminContactsTable
				contacts={contacts}
				onContactDeleteClick={handleContactDeleteClicked}
			/>

			<DeleteContactModal
				isOpen={isOpen}
				onClose={() => setIsOpen(false)}
				onDelete={handleContactDeleteConfirm}
				deleteLoading={contactDeleteLoading}
			/>
		</>
	);
};

export default AdminContactsList;
