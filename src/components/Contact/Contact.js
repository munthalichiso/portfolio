import React from "react";
import {library} from "@fortawesome/fontawesome-svg-core";
import {fab} from "@fortawesome/free-brands-svg-icons";
import {fas} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "../Link/Link";
library.add(fab, fas);

const Contact = ({title, text, link, baseSiteLink, icon, iconColour}) => {
	let [iconType, iconName] = icon.split(" ");

	return (
		<>
			<div className="p-1">
				<div className="bg-dark-650 p-6 rounded-lg text-white">
					<Link
						text={<FontAwesomeIcon
							icon={[iconType, iconName]}
							className="mx-auto mb-2 w-12 h-12"
							color={"#" + iconColour}
							style={{
								width: "3rem",
								height: "3rem"
							}}
						/>}
						href={baseSiteLink}
					/>

					<h2 className="text-lg font-medium title-font my-1">{title}</h2>

					<div>
						<Link text={text} href={link}
							textClass={`${link ? "text-blue-400" : "text-white"} text-lg`}/>
					</div>
				</div>
			</div>
		</>
	);
};

export default Contact;
