import React from "react";
import Contact from "./Contact";
import useEndpoint from "../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";

const ContactList = () => {
	const {contacts, loading: contactsLoading} = useEndpoint("/api/contact", "contacts");

	if (contactsLoading) {
		return <div className="grid grid-cols-1 text-center px-8 gap-4 lg:grid-cols-3 lg:px-32">
			{Array(3).fill(null).map((d, i) => {
				return <Skeleton
					key={i}
					height="12rem"
				/>;
			})}
		</div>;
	}

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 lg:grid-cols-3 lg:px-32">
			{contacts.map(contact => {
				return <Contact
					key={contact.id}
					title={contact.title}
					text={contact.text}
					link={contact.link}
					baseSiteLink={contact.baseSiteLink}
					icon={contact.icon}
					iconColour={contact.iconColour}
				/>;
			})}
		</div>
	);
};

export default ContactList;
