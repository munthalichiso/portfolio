import Joi from "joi";

export default Joi.object({
	name: Joi.string()
		.max(128)
		.required()
		.label("Name"),
	type: Joi.string()
		.max(128)
		.required()
		.label("Type"),
	link: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.required()
		.label("Link"),
	imageUrl: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.required()
		.label("Image URL")
});
