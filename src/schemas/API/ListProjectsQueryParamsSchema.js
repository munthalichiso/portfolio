import Joi from "joi";

export default Joi.object({
	page: Joi.number()
		.positive()
		.default(1),
	sort: Joi.string()
		.valid("id", "name", "openSource", "dateAdded")
		.default("dateAdded"),
	sortDirection: Joi.string()
		.valid("ASC", "DESC")
		.uppercase()
		.default("DESC"),
	filterType: Joi.string()
		.valid("openSource"),
	filterValue: Joi.any()
		.when("filterType", {
			is: "openSource",
			then: Joi.boolean()
				.required()
		}),
	skillId: Joi.string()
		.length(22)
});
