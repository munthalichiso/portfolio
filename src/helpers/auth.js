import {compare} from "bcrypt";
import jwt from "jsonwebtoken";

export const compareHash = (password, hash) => {
	return compare(password, hash);
};

export const signJwtAsync = (payload, expiresIn = "15m") => {
	return new Promise((resolve, reject) => {
		let signingOptions = {
			issuer: process.env.JWT_ISSUER,
			audience: process.env.JWT_AUDIENCE,
			algorithm: "RS256"
		};

		if (!payload.exp)
			signingOptions.expiresIn = expiresIn;

		jwt.sign(payload, process.env.JWT_PRIVATE_KEY, signingOptions, (err, token) => {
			if (err)
				return reject(err);

			return resolve(token);
		});
	});
};

export const verifyJwtAsync = (token, ignoreExpiration = false) => {
	return new Promise((resolve, reject) => {
		let verifyOptions = {
			issuer: process.env.JWT_ISSUER,
			audience: process.env.JWT_AUDIENCE,
			algorithms: ["RS256"],
			ignoreExpiration
		};

		jwt.verify(token, process.env.JWT_PUBLIC_KEY, verifyOptions, function (err, decoded) {
			if (err)
				return reject(err);

			return resolve(decoded);
		});
	});
};
