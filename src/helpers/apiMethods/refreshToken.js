import axios from "axios";
import wrapAxiosRequest from "./wrapAxiosRequest";

const refreshToken = async () => {
	return wrapAxiosRequest(axios.post("/api/auth/refresh"));
};

export default refreshToken;
