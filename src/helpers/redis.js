import Redis from "ioredis";

const redis = new Redis(process.env.REDIS_URI);

export const get = (key) => {
	return redis.get(key);
};

export const getJson = async (key) => {
	let value = await get(key);

	try {
		return JSON.parse(value);
	} catch (err) {
		return null;
	}
};

export const setex = (key, value, ttl) => {
	return redis.setex(key, ttl, value);
};

export const setexJson = (key, value, ttl) => {
	return setex(key, JSON.stringify(value), ttl);
};
